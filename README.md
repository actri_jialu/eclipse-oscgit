## 码云 - Git@OSC plugin for eclipse 4.5
Allow works with [码云 - Git@OSC](https://git.oschina.net)
## Installation
* Eclipse Plugins Update Site http://kesin.oschina.io/update-site/4.5/

## Vendor
* Droid - [GitOSC](https://git.oschina.net/droidqw) - [GitHub](https://github.com/droid-Q/)

## Authors
* 码云 - [GitOSC](https://git.oschina.net/oschina/intellij-gitosc)
* [Jiaqi Gu](droidqw@gmail.com)

##Feedback
* droidqw@gmail.com